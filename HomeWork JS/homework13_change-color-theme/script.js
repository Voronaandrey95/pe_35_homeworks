const iconEnterPass = document.getElementsByClassName('fas')[0]
const iconConfirmPass =  document.getElementsByClassName('fas')[1]
const inputEnterPass = document.getElementsByTagName('input')[0]
const inputConfirmPass = document.getElementsByTagName('input')[1]
const btn = document.querySelector('.btn')
function removeVisiblePass (inp,icn){
    inp.type = 'password'
    icn.classList.remove('fa-eye-slash')
    icn.classList.add('fa-eye')
}
iconEnterPass.addEventListener('click', function (){
    inputEnterPass.type = 'text'
    iconEnterPass.classList.remove('fa-eye')
    iconEnterPass.classList.add('fa-eye-slash')
    setTimeout(removeVisiblePass, 1500,inputEnterPass,iconEnterPass)
})
iconConfirmPass.addEventListener('click', function (){
    inputConfirmPass.type = 'text'
    iconConfirmPass.classList.remove('fa-eye')
    iconConfirmPass.classList.add('fa-eye-slash')
    setTimeout(removeVisiblePass, 1500,inputConfirmPass,iconConfirmPass)
})
btn.addEventListener('click',function (){
    if (inputEnterPass.value === inputConfirmPass.value){
        alert(`You are welcome`)
    } else {
        alert(`Нужно ввести одинаковые значения`)
        inputEnterPass.value =''
        inputConfirmPass.value =''
    }
})

