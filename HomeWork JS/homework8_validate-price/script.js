'use strict'
const sectionPrice = document.createElement('section')
const spanPrice = document.createElement('span')
const spanClose = document.createElement('span')
const spanNoCorrect = document.createElement('span')
const mainSection = document.querySelector('.main-section')
const blockPrice= document.querySelector('.block-price')
const input = document.querySelector('input')
sectionPrice.classList.add('radius')
spanClose.classList.add('radius')
spanNoCorrect.classList.add('radius')
//// style
sectionPrice.prepend(spanClose);
sectionPrice.prepend(spanPrice)
sectionPrice.style.border = '1px solid black'
sectionPrice.style.display = 'flex'
sectionPrice.style.justifyContent = 'space-around'
sectionPrice.style.padding = '2px'
sectionPrice.style.width = `${blockPrice.clientWidth}px`
spanClose.innerHTML = '&#10008;'
spanClose.style.cursor = 'pointer'
spanClose.style.border = '1px solid black'
spanNoCorrect.style.border = '1px solid black'
spanNoCorrect.style.width = `${blockPrice.clientWidth}px`
spanNoCorrect.style.textAlign = `center`


function bordersPrice(){
    blockPrice.style.borderColor = 'green'}
function checkPrice(){
    blockPrice.style.borderColor = 'grey'
    if (input.value >= 0){
        spanNoCorrect.remove()
        spanPrice.textContent = `${input.value}`
        mainSection.prepend(sectionPrice)
        input.style.color = 'green'
    }else if (input.value <0) {
        sectionPrice.remove()
        spanNoCorrect.textContent = 'Please enter correct price'
        mainSection.append(spanNoCorrect)
        input.style.color = 'black'
    }
    spanClose.addEventListener('click',()=>{
        sectionPrice.remove()
        input.value = ''
    })
}

input.addEventListener('focus', bordersPrice)
input.addEventListener('blur', checkPrice)

//
// console.log(mainSection)
// console.log(sectionPrice)
// console.log(spanPrice)
// console.log(spanClose)
// console.log(spanNoCorrect)
// console.log(input.value)