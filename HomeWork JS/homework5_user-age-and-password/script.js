'use strict'
function createNewUser() {
    const firstName = prompt("Enter your name");
    const lastName = prompt("Enter your surname");
    // let birthdays
    let newUser = {};

    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        writable:true,
        configurable:true,
        get function() {
            return this.firstName;
        },
        set function (params) {
            return this.firstName = params;
        },
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        writable:true,
        configurable:true,
        get function() {
            return this.lastName;
        },
        set function(newValue) {
            return this.lastName = newValue;
        },
    });
    newUser.setFirstName = function(value){
        Object.defineProperty(newUser, 'firstName', {
            writable:true,
            configurable:true,
            value: value,
            get function() {
                return this.firstName;
            },
            set function(value) {
                return this.firstName = value;
            },
        });
    }
    newUser.setBirthday = function (){
        // do {
            let birthday
            birthday = prompt('Введите дату рождения', 'dd.mm.yyyy');
            birthday = Array.from(birthday)
            // console.log(birthday)
        // } while (birthday[2] !=='.' || birthday[5] !=='.' || birthday.length !==10 || birthday[0] >3 || (birthday[0] === 3 || birthday[1] > 1) || birthday[3]>1 )
        birthday = `${birthday[0]}${birthday[1]}.${birthday[3]}${birthday[4]}.${birthday[6]}${birthday[7]}${birthday[8]}${birthday[9]}`
        this.birthday = birthday;
    }
    newUser.getAge = function (){
        let now = new Date();
        let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        let arrayBirthday = Array.from(this.birthday)
        // console.log(arrayBirthday)
        let dayBirthday = +`${arrayBirthday[0]}${arrayBirthday[1]}`
        // console.log(dayBirthday)
        let monthBirthday = +`${arrayBirthday[3]}${arrayBirthday[4]}` - 1
        // console.log(monthBirthday)
        let yearBirthday = +`${arrayBirthday[6]}${arrayBirthday[7]}${arrayBirthday[8]}${arrayBirthday[9]}`
        // console.log(yearBirthday)
        let realBirthday = new Date (yearBirthday, monthBirthday, dayBirthday);
        // console.log(realBirthday)
        let thisYearBirthday = new Date(today.getFullYear(), monthBirthday, dayBirthday);
        let age
        // console.log(thisYearBirthday)
        age = today.getFullYear() - yearBirthday;
        if (today < thisYearBirthday){
            age = age -1
        }
        this.age = age
        console.log(age)
    }
    newUser.getPassword = function (){

        let password = (this.firstName[0].toUpperCase().concat(this.lastName.toLowerCase(), this.birthday.slice(6)))
        this.password = password
        console.log(password)
    }
    newUser.getLogin = function () {
        const userLogin = this.firstName[0].toLowerCase().concat(this.lastName.toLowerCase())
        console.log(userLogin);
    }
    return newUser;
};

const user = createNewUser();
user.setBirthday()
user.getAge()
user.getLogin()
// console.log(user.birthday)
// console.log(+user.birthday.substr(6,4))
user.getPassword()
console.log(user)

// console.log(user);
// user.setBirthday();
// console.log(user.birthday)
// console.log(user)
// user.firstName = "Igor"
// user.setFirstName("Hero");
// console.log(user);
// user.firstName = "Igor"
// console.log(user);
// user.getLogin();
// console.log(user)