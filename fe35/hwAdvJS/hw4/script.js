const root = document.getElementById('film_name')
const filmHeroes = document.getElementById('film_heroes')
const url = 'https://ajax.test-danit.com/api/swapi/films'
class InfoFilms {
    constructor(url){
        this.url = url
    }
    request(url,characters =''){
        return axios.get(url+characters)
    }
    allFilms(){
        this.request(this.url)
        .then(({data}) =>{
            const titleFilm =document.createElement('ul')
            titleFilm.classList.add('list')
            const nameFilm = data.map(({name, episodeId, openingCrawl, id}) => {
             const nameFilm = document.createElement('li');
             const names = document.createElement('p');
             names.classList.add('nameFilm')
            //  names.dataset.attribute = `${id}`
             names.textContent = `Epizode: ${episodeId} -  ${name}`
             const descriptionFilm = document.createElement('p')
             descriptionFilm.textContent = `Description: ${openingCrawl}`
            const btnHero = document.createElement('button')
            btnHero.textContent = `show heroes`
            nameFilm.append( names,descriptionFilm,btnHero)
            titleFilm.append(nameFilm)
            btnHero.addEventListener('click',()=>{
                if (!btnHero.classList.contains('active')){
                    if(document.getElementById(`heroes${id}`)===null){
                        btnHero.classList.add('active')
                        this.heroFilms(id,nameFilm)
                    } else {
                        btnHero.classList.add('active')
                        document.getElementById(`heroes${id}`).style.display = 'block'
                    }
                    btnHero.textContent = 'closed Heroes'
                    
                    console.log(id);
                } else if(btnHero.classList.contains('active')){
                    document.getElementById(`heroes${id}`).style.display = 'none'
                    btnHero.classList.remove('active')
                    btnHero.textContent = `show heroes`
                }
                
                
            })
         });
         root.append(titleFilm)

        })
    }

    heroFilms(id,element){
        const infoFilm=[]
        this.request(this.url)
        .then(({data}) =>{
            console.log(data);

            data.map((data)=>{
                if(data.id === id){
                const nameFilm = document.createElement('p');
                nameFilm.id = `heroes${id}`
                let  arrHeroes= []
                    data.characters.map((characters)=>
                        this.request(characters)
                        .then(({data})=>{

                            arrHeroes.push(data.name)
                            nameFilm.textContent = `${arrHeroes.join(', ')}`
                            
                        })
                        .then(()=>{
                            element.append(nameFilm)
                        })
                        
                    ) 
                    
                    
                   }
            })
        })
        this.infoAllFilms = infoFilm
    
    }
}

const starWars = new InfoFilms(url)


starWars.allFilms()    // СПИСОК ФИЛЬМОВ
// console.log(allFilmsName);


// starWars.heroFilms()                   // Список всех фильмов с их героями










