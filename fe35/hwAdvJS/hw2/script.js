const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
const root = document.querySelector('#root')
 function getBooks(title){
   const span = document.createElement('span')
   for (let i=0;i<title.length;i++){
    const ul = document.createElement('ul')
    try{  
      if (!title[i].hasOwnProperty('price')){
        messageError('price')
      }      
      if (!title[i].hasOwnProperty('name')){
        messageError('name')
      }      
      if (!title[i].hasOwnProperty('author')){
        messageError('author')
      }
      const ulSub = document.createElement('ul')
      let liName = document.createElement('li');
      liName.textContent = `${title[i].name}`
      let liAuthor = document.createElement('li');
      liAuthor.textContent = `${title[i].author}`
      let liPrice = document.createElement('li');
      liPrice.textContent = `${title[i].price}`
      ul.append(liName)
      ul.append(liAuthor)
      ul.append(liPrice)
      span.append(ul)
    }
    catch(e){
      console.log(e.message);
      // return
    }
  }
  
  root.append(span)
}

getBooks(books)
