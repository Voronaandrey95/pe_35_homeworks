"use strict"
class Employee {
    constructor (name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary
    }
    get _name(){
        return this.name
    };
    get _age(){
        return this.age
    };
    get _salary(){
        return this.salary
    };
    set _name(value){
        this.name = value
    }
    set _age(value){
        this.age = value
    }
    set _salary(value){
        this.salary = value
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang ){
        super(name, age, salary,);
        this.lang = lang;
    }
    get _salary(){
        return this.salary * 3;
    };
    set _salary(value){
        this.salary = value
    };
}
const Voron = new Programmer ("Andrey", "24", 500, 'eng,rus');
const Dark = new Programmer ("Dark", "74", 350, 'eng,rus,ua');
console.log(Voron);
console.log(Voron._salary);
