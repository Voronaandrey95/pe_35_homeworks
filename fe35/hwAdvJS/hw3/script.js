'use strict'

// TASK1

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// let newClients = [...clients1, ...clients2];
// newClients = Array.from(new Set(newClients))
// console.log(newClients);



// TASK2

// const characters = [
//     {
//       name: "Елена",
//       lastName: "Гилберт",
//       age: 17, 
//       gender: "woman",
//       status: "human"
//     },
//     {
//       name: "Кэролайн",
//       lastName: "Форбс",
//       age: 17,
//       gender: "woman",
//       status: "human"
//     },
//     {
//       name: "Аларик",
//       lastName: "Зальцман",
//       age: 31,
//       gender: "man",
//       status: "human"
//     },
//     {
//       name: "Дэймон",
//       lastName: "Сальваторе",
//       age: 156,
//       gender: "man",
//       status: "vampire"
//     },
//     {
//       name: "Ребекка",
//       lastName: "Майклсон",
//       age: 1089,
//       gender: "woman",
//       status: "vempire"
//     },
//     {
//       name: "Клаус",
//       lastName: "Майклсон",
//       age: 1093,
//       gender: "man",
//       status: "vampire"
//     }
//   ];
  
// let newObj = []

// function charactersShortInfo(obj) {
//         obj.forEach(elem => {
//             let {name, lastName, age} = elem
//             newObj.push({name: name, lastName: lastName, age: age})
//     });
// }
// charactersShortInfo(characters)
// console.log(newObj);



// TASK3

// const user1 = {
//     name: "John",
//     years: 30
//   };

// const {name, age = user1.years, isAdmin = "false"} = user1;
// console.log(name, age, isAdmin);



// TASK4

// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//       lat: 38.869422, 
//       lng: 139.876632
//     }
//   }
  
//   const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
//   }
  
//   const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto', 
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
//   };

//   let actualProfile = {}
//   function fullProfile(newObj,obj1,obj2,obj3) {
//       let arrObj = [obj1,obj2,obj3]
//       arrObj.forEach(elem => {
//           for (const key in elem) {
//                 newObj[`${key}`] = elem[key]
//               }
//           })
//   }

//   fullProfile(actualProfile,satoshi2018,satoshi2019,satoshi2020)

//   console.log(actualProfile);


// TASK5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
//   }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
//   }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
//   }];
  
//   const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
//   }

//   const newTitle = books.concat(bookToAdd)
//   console.log(newTitle);


// TASK6

// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
//   }

// class NewEmployee {
//     constructor(obj,age,salary){
//         this.name = obj.name;
//         this.surname = obj.surname;
//         this.age = age;
//         this.salary = salary
//     }
// }

// const fullEmployee = new NewEmployee(employee,20,'100000$')
// console.log(fullEmployee);


// TASK7

// const array = ['value', () => 'showValue'];
// const test   = [...array]

// console.log(test);
// console.log(array);

// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'
