import Form from "./modules/Form.js";
import Request from "./modules/Request.js";
import Modal from "./modules/Modal.js";
import CONSTANTS from "./constants.js";
import Article from "./modules/Article.js";

const { BASE_URL, FORM_TEMPLATE } = CONSTANTS;

const articlesList = document.querySelector("#articles");
const root = document.querySelector("#root");
const create = document.querySelector(".create");

const request = new Request(BASE_URL);
const form = new Form(FORM_TEMPLATE).render();
const modalWindow = new Modal("id", ["modal", "modal1"], "Hello", form);

form.addEventListener("submit", (e) => {
    e.preventDefault();
    formHandler();
    event.target.closest(".modal").classList.remove("active");
});

create.addEventListener("click", () => {
    modalWindow.render(root);
    modalWindow.openModal();
});

getAllArticles();

async function getAllArticles() {
    const articles = await request.get("/posts");

    articles.data.forEach((element) => {
        const article = new Article(element);
        const articleEditForm = article.editPostHandler().formHTML;
        const articleFormObj = article.editPostHandler().objectForm;
        articleEditForm.id += "-" + element.id;
        console.log(articleEditForm);

        articleEditForm.addEventListener("submit", (e) => {
            e.preventDefault();

            let li = e.target.parentNode;
            let articleID = li.getAttribute("data-id");
            const request = new Request(BASE_URL);

            request
                .put("/posts/", articleID, articleFormObj.getData())
                .then(({ data }) => {
                    const currArticle = document.querySelector(
                        `[data-id="${articleID}"]`
                    );

                    currArticle.outerHTML = new Article(
                        data
                    ).render().outerHTML;
                });
            event.target.closest(".modal").classList.remove("active");
        });

        articlesList.append(article.render());
    });
}

function formHandler() {
    if (
        title.value !== "" &&
        intro.value !== "" &&
        text.value !== "" &&
        date !== "" &&
        author !== ""
    ) {
        const post = {
            title: title.value,
            intro: intro.value,
            text: text.value,
            date: date.value,
            author: author.value,
        };
        request
            .post("/posts", post)
            .then((e) => {
                console.log(e);
            })
            .catch((er) => {
                console.log(er);
            });
    }
}
