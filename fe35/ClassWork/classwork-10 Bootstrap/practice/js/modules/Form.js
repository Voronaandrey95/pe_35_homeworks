export default class Form {
    constructor(formTemplate) {
        this.formTemplate = formTemplate;
    }
    render(
        titleContent = "",
        introContent = "",
        textContent = "",
        dateContent = "",
        authorContent = ""
    ) {
        const fTpl = this.formTemplate.content;
        const form = fTpl.querySelector("form").cloneNode(false);
        const title = fTpl.querySelector("#title").cloneNode(false);
        title.value = titleContent;
        const intro = fTpl.querySelector("#intro").cloneNode(false);
        intro.value = introContent;
        const text = fTpl.querySelector("#text").cloneNode(false);
        text.value = textContent;
        const date = fTpl.querySelector("#date").cloneNode(false);
        date.value = dateContent;
        const author = fTpl.querySelector("#author").cloneNode(false);
        author.value = authorContent;
        const submit = fTpl.querySelector('[type = "submit"]').cloneNode(false);

        form.append(title, intro, text, date, author, submit);

        return form;
    }
    getData() {
        return {
            title: title.value,
            intro: intro.value,
            text: text.value,
            date: date.value,
            author: author.value,
        };
    }
}
