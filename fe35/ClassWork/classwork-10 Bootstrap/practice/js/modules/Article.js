import Element from "./Element.js";
import Modal from "./Modal.js";
import Form from "./Form.js";
import CONSTANTS from "../constants.js";
import Request from "./Request.js";
import CommentForm from "./Comment.js";

const { BASE_URL, FORM_TEMPLATE } = CONSTANTS;

export default class Article {
    constructor({ title, intro, text, date, author, id }) {
        this.title = title;
        this.intro = intro;
        this.text = text;
        this.date = date;
        this.author = author;
        this.id = id;

        this.btnEdit = new Element("button", "Edit", "", [
            "edit",
        ]).createElement();
    }
    editPostHandler() {
        const form = new Form(FORM_TEMPLATE);
        const formHTML = form.render(
            this.title,
            this.intro,
            this.text,
            this.date,
            this.author
        );
        this.btnEdit.addEventListener("click", () => {
            console.log(formHTML);
            const modalWindow = new Modal("mod", ["modal"], "edit", formHTML);
            modalWindow.render();
            modalWindow.openModal();
        });
        return {
            objectForm: form,
            formHTML: formHTML,
        };
    }
    render() {
        const title = new Element("h3", this.title).createElement();
        const introText = new Element("p", this.intro).createElement();
        const text = new Element("p", this.text).createElement();
        const date = new Element("p", this.date).createElement();
        const author = new Element("p", this.author).createElement();

        const addComment = new Element("button", "Add Comment", "", [
            "addcomment",
        ]).createElement();

        const fragment = document.createDocumentFragment();
        fragment.append(
            title,
            introText,
            text,
            date,
            author,
            this.btnEdit,
            addComment
        );
        const li = new Element("li", fragment).createElement();
        li.dataset.id = this.id;

        addComment.addEventListener("click", (e) => {
            let li = e.target.parentNode;
            let articleNumber = li.getAttribute("data-id");
            const formComment = new CommentForm(articleNumber).render();
            const modalWindow = new Modal(
                "mod",
                ["modal"],
                "edit",
                formComment
            );
            modalWindow.render();
            modalWindow.openModal();
        });

        return li;
    }
}
