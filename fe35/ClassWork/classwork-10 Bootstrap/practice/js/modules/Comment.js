import Request from "./Request.js";
import CONSTANTS from "../constants.js";

const { BASE_URL } = CONSTANTS;
export default class CommentForm {
    constructor(postId) {
        this.postId = postId;
    }
    render() {
        const form = document.createElement("form");
        const text = document.createElement("input");

        const date = document.createElement("input");
        date.type = "date";
        const author = document.createElement("input");
        const sbmit = document.createElement("input");
        sbmit.type = "submit";
        let that = this;
        form.addEventListener("submit", (e) => {
            e.preventDefault();

            const request = new Request(BASE_URL);
            request
                .post("/comments", {
                    text: text.value,
                    author: author.value,
                    date: date.value,
                    postid: that.postId,
                })
                .then(({ data }) => {
                    console.log(data);
                    let blockqoute = document.createElement("blockquote");
                    const { text, author, date } = data;
                    let pText = document.createElement("p");
                    pText.textContent = text;
                    let pAuthor = document.createElement("p");
                    pAuthor.textContent = author;
                    let pDate = document.createElement("p");
                    pDate.textContent = date;
                    blockqoute.append(pText, pAuthor, pDate);
                    document
                        .querySelector(`[data-id="${that.postId}"]`)
                        .append(blockqoute);
                });
            e.target.closest(".modal").classList.remove("active");
        });
        form.append(text, author, date, sbmit);
        return form;
    }
}
