import CONSTANTS from "./constants.js";

const { ROOT } = CONSTANTS;

export default class Card {
    constructor(url) {
        this.url = url;
    }

    createCard({ avatar_url, login, url, followers_url }) {
        const cardCell = document.createElement("div");
        cardCell.classList.add("col-3", "my-2");

        const card = document.createElement("div");
        card.classList.add("card");

        const cardBody = document.createElement("div");
        cardBody.classList.add("card-body");

        const avatar = document.createElement("img");
        avatar.classList.add("card-img-top");
        avatar.src = avatar_url;

        const userLogin = document.createElement("p");
        userLogin.textContent = login;

        const repoUrl = document.createElement("p");
        repoUrl.textContent = url;

        const btn = document.createElement("button");
        btn.textContent = "See folowers";
        btn.classList.add("btn", "btn-primary");

        btn.addEventListener("click", () => {
            this.renderFolowers(followers_url);
        });

        cardBody.append(userLogin, repoUrl, btn);
        card.append(avatar, cardBody);
        cardCell.append(card);

        return cardCell;
    }

    renderFolowers(followersUrl) {
        this.request(followersUrl).then(({ data }) => {
            console.log(data);
            const cardFollowers = data.map(({ login }) => `<li>${login}</li>`);

            console.log(cardFollowers);

            // (сюда выводить логины фоловеров).innerHTML += "<ul>" + cardFollowers.join(" ") + "</ul>";
        });
    }

    render(renderType) {
        const container = document.createElement("div");
        container.classList.add("container");

        const row = document.createElement("div");
        row.classList.add("row");
        container.append(row);
        ROOT.append(container);

        let start = Date.now();

        this.request(this.url + "users").then(({ data }) => {
            console.log(data[0]);

            const cardsList = data.map((data) => this.createCard(data));

            console.log(cardsList);
            // console.log(type);

            if (renderType === "byOne") {
                try {
                    cardsList.forEach((data) => {
                        row.append(data);

                        let end = Date.now();
                        console.log(end - start);
                        const form = confirm("Continue?");

                        if (!form) {
                            throw "err";
                        }
                    });
                } catch (err) {
                    console.log(err);
                }
            }
            if (renderType === "all") {
                const fragment = document.createDocumentFragment();
                fragment.append(...cardsList);
                row.append(fragment);

                let end = Date.now();
                console.log(end - start);
            }
        });
    }

    request(url) {
        return axios.get(url);
    }
}
