import Card from "./Card.js";
import CONSTANTS from "./constants.js";

const { BASE_URL } = CONSTANTS;

let card = new Card(BASE_URL);

const byOne = document.querySelector("#byOne");
const all = document.querySelector("#all");

byOne.addEventListener("click", card.render.bind(card, "byOne"));
all.addEventListener("click", card.render.bind(card, "all"));
