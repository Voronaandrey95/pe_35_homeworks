'use strict'

// deletePost()
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((e) => {
//         console.log(e.message);
//     })
//     .finally(() => {
//         getPosts();
//     });

// const newPost = {
//     title: "New post 2",
//     author: "my post",
//     postText: "lorem ipsum sit amet",
// };

// function getPosts() {
//     axios.get(BASE_URL + "/posts").then(({ data }) => {
//         console.log(data);
//     });
// }

// // createPost(newPost);
// function createPost(post) {
//     return axios({
//         method: "post",
//         url: BASE_URL + "/posts",
//         data: post,
//         headers: {
//             "Content-Type": "application/json",
//         },
//     });
//     //
//     return axios.post(BASE_URL + "/posts", post);
// }

// function deletePost(id = 1) {
//     return axios.delete(BASE_URL + "/posts/" + id);
// }

const formTemplate = document.getElementById("create-post");
console.log(formTemplate);
const request = new Request(BASE_URL)
class Form {
    constructor(formTemplate) {
        this.formTemplate = formTemplate;
    }
    render(root) {
        const fTpl = this.formTemplate.content;
        const form = fTpl.querySelector("form");
        console.log(form);
        form.addEventListener('submit', request.post('') )
        const title = fTpl.querySelector("#title");
        const intro = fTpl.querySelector("#intro");
        const text = fTpl.querySelector("#text");
        const date = fTpl.querySelector("#date");
        const author = fTpl.querySelector("#author");
        const submit = fTpl.querySelector('[type = "submit"]');
        form.append(title, intro, text, date, author, submit);
        root.append(form);
    }
}

class Request {
    constructor(url){
        this.url = url
    }
    post(entity, data){
    return axios({
        method: "post",
        url: this.url + entity,
        data: data,
        headers: {
            "Content-Type": "application/json",
        },
    })
}   
}

const root = document.querySelector("#root");
const form = new Form(formTemplate);
const request = new Request(BASE_URL)
form.render(root);
