import Element from "./Element.js";
import Modal from "./Modal.js";
import Form from "./Form.js";
import CONSTANTS from "../constants.js";
import Request from "./Request.js";

const { BASE_URL, FORM_TEMPLATE } = CONSTANTS;
export default class Article {
    constructor({ title, intro, text, date, author, id }) {
        this.title = title;
        this.intro = intro;
        this.text = text;
        this.date = date;
        this.author = author;
        this.id = id;
    }

    render() {
        const title = new Element("h3", this.title).createElement();
        const introText = new Element("p", this.intro).createElement();
        const text = new Element("p", this.text).createElement();
        const date = new Element("p", this.date).createElement();
        const author = new Element("p", this.author).createElement();
        const btnEdit = new Element("button", "Edit", "", [
            "edit",
        ]).createElement();

        const fragment = document.createDocumentFragment();
        fragment.append(title, introText, text, date, author, btnEdit);
        const li = new Element("li", fragment).createElement();
        li.dataset.id = this.id;

        const articleID = this.id;
        btnEdit.addEventListener("click", () => {
            function articleFormHandler() {
                const request = new Request(BASE_URL);

                // request
                //     .put("/posts/", articleID, form.getData())
                //     .then((data) => {
                //         return request.get("/posts");
                //     })
                //     .then(({ data }) => {
                //         // rewrite ALL articles
                //         // const articlesList =
                //         //     document.querySelector("#articles");
                //         // articlesList.innerHTML = "";
                //         // data.forEach((element) => {
                //         //     articlesList.append(new Article(element).render());
                //         // });
                //     })
                //     .catch((e) => {
                //         console.log(e);
                //     });

                request
                    .put("/posts/", articleID, form.getData())
                    .then(({ data }) => {
                        // rewrite CURRENT article
                        const currArticle = document.querySelector(
                            `[data-id="${articleID}"]`
                        );

                        currArticle.outerHTML = new Article(
                            data
                        ).render().outerHTML;
                    });
            }
            const form = new Form(FORM_TEMPLATE, articleFormHandler);

            const modalWindow = new Modal(
                "mod",
                ["modal"],
                "edit",
                form.render(
                    this.title,
                    this.intro,
                    this.text,
                    this.date,
                    this.author
                )
            );
            modalWindow.render();
            modalWindow.openModal();
        });

        return li;
    }
}
