// function main() {
//     console.log("A"); // 1

//     setTimeout(function () {
//         console.log("setTimeout 1");
//     }, 3); // 5

//     setTimeout(function () {
//         console.log("setTimeout 2"); // 4
//     }, 2);

//     setTimeout(function exec() {
//         console.log("setTimeout 3"); // 3
//     }, 2);

//     runWhileLoopForNSeconds(5);
//     console.log("C"); // 2
// }
// main();

// function runWhileLoopForNSeconds(sec) {
//     let start = Date.now(),
//         now = start;
//     while (now - start < sec * 1000) {
//         now = Date.now();
//     }
//     console.log("runWhileLoopForNSeconds");
// }

export async function promce() {
    return 1;
}
// async function promce() {
//     return Promise.resolve(1);
// }

// promce().then((data) => {
//     console.log(data);
// });

// const BASE_URL = "https://ajax.test-danit.com/api/swapi/starships";

// export async function getPosts() {
//     try {
//         let responce = await axios.get(BASE_URL);
//         return responce.data;
//     } catch (e) {
//         return e.message;
//     }
// }

// export class User {}

// export default {
//     User,
//     getPosts,
//     promce,
// };
// export default "Text";
