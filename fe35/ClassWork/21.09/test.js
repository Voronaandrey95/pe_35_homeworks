'use strict'

const URI = "https://ajax.test-danit.com/api/swapi/planets";

// function getPlanets() {
//     return new Promise((resolve, reject) => {
//         const xhr = new XMLHttpRequest();
//         xhr.open("GET", URI);
//         xhr.send(null);

//         xhr.onreadystatechange = function () {
//             if (xhr.readyState == 4) {
//                 if (xhr.status == 200) {
//                     const response = JSON.parse(xhr.response);
//                     resolve(response);
//                 } else {
//                     reject(new Error("Error loading\n"));
//                 }
//             }
//         };
//     });
// }

// getPlanets()
//     .then((data) => {
//         console.log(data);
//     })
//     .catch((e) => {
//         console.log(e);
//     })
//     .finally(() => {
//         console.log("finally");
//     });

// ///////// FETCH
// function showAvatar() {
//     return fetch(`https://api.github.com/users/telegraf`, {
//         method: "GET",
//         headers: {
//             "Content-Type": "application/json",
//         },
//     });
// }

// showAvatar()
//     .then((response) => {
//         console.log(response);

//         // return  response.text();
//         //   return response.blob();
//         return response.json();
//     })
//     .then((data) => {
//         console.log(data);
//         const img = document.createElement("img");
//         img.src = data.avatar_url;
//         document.body.prepend(img);
//     })
//     .catch((e) => {
//         console.log(e.message);
//     });

// axios({
//     method: "GET",
//     url: "https://ajax.test-danit.com/api/swapi/films",
//     // }).then(function (response) {
// }).then(function ({ data, status, statusText, headers, config }) {
//     console.log(data);
//     console.log(status);
//     //   console.log(statusText);
//     //   console.log(headers);
//     //   console.log(config);
//     //  console.log(response);
// });

// axios
//     .get("https://ajax.test-danit.com/api/swapi/films", {})
//     .then(function ({ data }) {
//         console.log(data);
//     });


