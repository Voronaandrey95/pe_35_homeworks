'use strict'
// const timer = setTimeout(timerCallBack, 1500)
// const startTime = new Date();
//
// function timerCallBack (){
//     const finishTime = new Date()
//     console.log('timer alarm');
//     console.log(finishTime - startTime)
//     clearTimeout(timer)
// }

// const infiniteTimer = setInterval(infiniteTimerCallBack, 1000)
//
// function infiniteTimerCallBack (){
//     console.log('timer alarm');
//
// }

///// Local Storage
// const user = {
//     age: 18,
//     name: 'Uasy',
// }
// localStorage.clear()
//
// // JavaScript Object Notation ====> вариант обмена данных. Сохраняет обьекты в качестве строки \\\\\
// const userJSON = JSON.stringify(user)
// console.log(userJSON)
// localStorage.setItem('user', userJSON)
// const userFromStorage = localStorage.getItem('user')
// console.log(userFromStorage)
// const newUser = JSON.parse(userFromStorage)
// console.log(newUser)
// //
// localStorage.setItem('age', age)
// sessionStorage.setItem('age', age)
// localStorage.getItem('age')
// sessionStorage.getItem('age')
//
// console.log(localStorage)
// console.log(sessionStorage)
// localStorage.removeItem('age')  // удаляет елемент указаный
// localStorage.clear() // чистит полностью localStorage
// console.log(localStorage.length)
// console.log(localStorage.key(0))
