'use strict';

// const user = {
//     name: 'Uasya',
//     _age: 18,
//     family: {
//         father:{
//             name: "Uasiliy",
//             getName: function (){
//                 return this.name;
//             },
//         },
//         mother: 'Uasilisa',
//     },
//
//     set age(value){
//         if (value > 0){
//             this._age = value;
//         }
//     },
//     get age(){
//         return this._age;
//     },
//
//     getName: function (){
//         return this.name;
//     },
// };
// console.log(user.family.father.getName());
// console.log(user.family.father.getName.call(user));
//
// console.log()
//
// // console.log(user);
// // console.log(user.getName);
// console.log(user.getName());
//
// user.maxAge = 100;
// console.log(user);
// user.age = 90
// console.log(user.age)
////////////////////////////////////////////////// COPY Object
// const user2 ={};
// for (const key in user){
//     user2[key] = user [key];
// }
// console.log(user2);
// console.log(user.name);
// user2.name = 'Vitalic';
// console.log(user.name);
// console.log(user2.name);
//
// console.log(user2['getName']());
// console.log(`${user2['getName']}`);


//////////////////////// Еще способ
//
// const user3 = Object.assign({}, user, user2);
//
// console.log(user3.name)
///////////////////////////// Самый простой способ копировать))
// const user3 = Object(user2);
// console.log(user3);


/////////////////////TASK 1///////////////////
function createUser(){
    return{
        name: prompt('Введи имя'),
        lastName: 'Vorona',
        jop: 'man',
        sayHi: function (){
            console.log(`Привет. Меня зовут ${this.name} ${this.lastName}`)
        },
        changeProperty: function (propertyName, propertyValue){
            if (Object.hasOwnProperty.call(this, propertyName)){
                this[propertyName] = propertyValue;
                console.log(`Yeah`)
            }else   {
                console.log(`Error`)
            }
        },
        creatProperty: function (propertyName, propertyValue){
            if (!Object.hasOwnProperty.call(this, propertyName)){
                this[propertyName] = propertyValue;
                console.log(`Yeah`)
            }else   {
                console.log(`Error`)
            }
        }
    };
}
// createUser().sayHi();
const user1 = createUser();
user1;

// user.sayHi();
user1.changeProperty('name', 'Alexa');
console.log(user1);
/////////////////////////////////////////TASK 2 ///////////////////
