'use strict'
/////////////////////////////////// Стрелочные функции

// function summ (a, b){
//     return a + b;
// }

// const summ = (a, b) => {
//     //to do
//     return a + b;
// }

// const   summ = (a, b) => a + b;

// const getObj = () => ({name: Uasya});

// const summ = (a=0, b=0) => a+b;
// console.log(summ(1));

// const   summAllNums = (...params) =>{   //  ... Спред оператор, только в параметрах Ю записывает в масив
//     console.log(params);
//     console.log(...params);  // Спред в любом месте - развигает границы структуры
// };
// console.log(summAllNums(1, 2, 3, 4, 5));
// const   user = {
//     name: 'Uasy'
// };
// const user2 = { ...user, age: 18};
// console.log(user2);

// console.log([1, 2, 3, 4, 5,['a', 'b', 'c']])
// console.log([1, 2, 3, 4, 5,...['a', 'b', 'c']])


// const sum = (...params) => {
//     let answ = 0;
//     if (params.length < 2) {
//         return `error`
//     } else {
//         for (let i = 0; i < params.length; i ++){
//             if (isNaN(params[i]) || typeof params[i] !== 'number'){
//                 return `error2 in ${i}`;
//             }
//         }
//         for (let j=0 ; j < params.length; j++){
//             answ +=params[j];
//         }
//         return answ
//     }
// }
// console.log(sum(4,6,8));

//            TASK 3
//
// const getMaxNumber= (...params) => {
//     if (params.length < 2) {
//         return `error`
//     } else {
//         for (let i = 0; i < params.length; i++) {
//             if (isNaN(params[i]) || typeof params[i] !== 'number') {
//                 return `error2 in ${i}`;
//             }
//         }
//         return  Math.max(...params);
//     }
// }
// console.log(getMaxNumber(5,4,5,9,9));

// TASK 4

// const getLog = (message, spin) => {
//     if (typeof (message) != 'string' || isNaN(spin) || typeof (spin) !== 'number') {
//         console.log(`errors`); return;
//     }
//     for (let i=0; i < spin; i++){
//         console.log(message)
//     }
// }
// getLog('sada', 9)

////////////// TASK 5
const add = (a, b) => a + b;
const subtract = (a, b) => a - b;
const multiply = (a, b) => a * b;
const divide = (a, b) => a / b;

const calc = (a, b, operation) => {
    return operation(a, b); /// callback function
};

console.log(calc(2, 4, add));

