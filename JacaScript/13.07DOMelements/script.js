'use strict';

const root = document.getElementById('root')
// console.log(root)
// const paragraph = document.createElement('p');
// console.log(paragraph);
//
// paragraph.classList.add('text');  // добавляет класс
// paragraph.style.fontSize = '30px';
// paragraph.textContent = 'Lorem ipsum dolor sit amet.';
//
// paragraph.setAttribute('data-number', '1');
// paragraph.setAttribute('id', 'big-text');// переписывает атрибут (класс, йд)
// // paragraph.setAttribute('data-size', '30px')
// paragraph.dataset.size = '30px'

// console.log(paragraph.getAttribute('id')) // возвращает указаный атрибут
// root.append(paragraph) // добавляет элемент в конце
// root.prepend(paragraph)// добавляет элеент в начале
// root.after(paragraph)// добавляет элеент  после
// root.before(paragraph)// добавляет элеент до

// paragraph.remove();
// root.before('<strong>strong here</strong>')

// root.insertAdjacentElement("afterbegin", paragraph);
// root.insertAdjacentHTML('afterbegin', '<strong>strong here</strong>');
// root.insertAdjacentText('afterbegin', '<strong>strong here</strong>');

// paragraph.classList.add()
// paragraph.classList.remove()
// paragraph.classList.toggle(); // ищет аткой класс, сли нет такого - добавляет, в ином случае удаляе
// console.log(paragraph.classList.toggle('text-big'))

// const fragment = document.createDocumentFragment();// создли документ
// console.log(fragment)
//
//
// for (let i = 0; i < 5; i++){
//     const paragraph = document.createElement('p')
//     paragraph.classList.add('text')
//     paragraph.style.fontSize ='30px'
//     paragraph.textContent = 'Lorem ipsum dolor sit amet.'
//
//     fragment.append(paragraph) // вложили в документ
// }
// console.log(fragment)
// root.append(fragment)
// console.log(root)
// console.log(fragment)

