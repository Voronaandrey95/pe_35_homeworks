'use strict'


// TASK1/2
//
// let btn = document.createElement('button')
// btn.textContent = 'Войти'
// btn.addEventListener('click', () => {
//     alert('Добро пожаловать!')
// })
// document.body.append(btn)
// console.dir(btn)
//
// const  onMouseOver = function (){
//     alert('«При клике по кнопке вы войдёте в систему.»');
//     btn.removeEventListener("mouseover", onMouseOver)
// }
// btn.addEventListener("mouseover", onMouseOver)


///////TASK 3
const PHRASE = 'Добро пожаловать!';
function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`
}

// let rgb = getRandomColor()
const h1 = document.createElement('h1')
h1.textContent = PHRASE
document.body.append(h1)
const button = document.createElement('button')
button.textContent = 'Раскрыть'
h1.after(button)

const changeColor = function (){
    let h1Array = h1.textContent.split('').map(function (element){
        let span = document.createElement('span')
        span.textContent = element;
        span.style.color = getRandomColor();
        return span;
    })
    h1.innerHTML = '';
    h1.append(...h1Array)
}
button.addEventListener('click', changeColor)
// const mouseMove = function (){
document.addEventListener('mousemove', changeColor) //-------------------- TASK 4
// }
// document.body.removeEventListener('mousemove', changeColor)

