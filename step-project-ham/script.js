'use strict'
///// Our Services     ////////
const servBtn = document.querySelector('.services-ul')
const servContent = document.getElementsByClassName('services-contents')
servBtn.addEventListener('click', function (elem){
    for (let i = 0; i< servContent.length; i++){
        servContent[i].classList.remove('services-content-display')
        servBtn.children[i].classList.remove('services-active')
    }
    elem.target.classList.add('services-active')
    for (let i = 0; i< servContent.length; i++){
        if (elem.target.id === servContent[i].getAttribute('name')){
            servContent[i].classList.add('services-content-display')
        }
    }
})
// + MORE LOAD  ////////////////
const getImg = {
    img: ['img/services/loader/wordpress1.jpg','img/services/loader/wordpress2.jpg','img/services/loader/wordpress3.jpg','img/services/loader/wordpress4.jpg','img/services/loader/wordpress5.jpg','img/services/loader/wordpress6.jpg','img/services/loader/wordpress7.jpg','img/services/loader/wordpress8.jpg','img/services/loader/wordpress9.jpg','img/services/loader/wordpress10.jpg','img/services/loader/wordpress13.jpg','img/services/loader/wordpress14.jpg'],
    name: ['gd','wd','lp','wp']
}
const loader = document.querySelector('.loader')
console.log(loader)
const btnAmaz = document.querySelector('.btn-amaz')
const amazActive = document.querySelector('.amazLiActive').getAttribute('name')
btnAmaz.addEventListener('click', function (){
    btnAmaz.style.display = 'none'
    loader.style.display = 'block'
    setTimeout(function (){
        loader.style.display = 'none'
    const amazContentImg = document.querySelector('.amaz-contentImg')
    const amazContent = document.querySelector('.amaz-content')
    let clone = amazContentImg.cloneNode(true)
    let cloneImg = clone.querySelector('img')
    let atribute = 0
    const amazActive = document.querySelector('.amazLiActive').getAttribute('name')
    for (let i = 0; i < getImg.img.length; i++){
        let clone = amazContentImg.cloneNode(true)
        let cloneImg = clone.querySelector('img')
        cloneImg.src = `${getImg.img[i]}`
        if (atribute > 3){
            atribute = 0
        }
        clone.setAttribute('name',`${getImg.name[atribute]}`)
        atribute ++
        if  (clone.style.display = 'none'){
            if (clone.getAttribute('name') === amazActive){
                clone.style.display = 'block'
            }
        }
        if (amazActive === 'amazAll'){
            clone.style.display = 'block'
        } else if (clone.getAttribute('name') !== amazActive){
            clone.style.display = 'none'
        }
        amazContent.append(clone)
    }
    // btnAmaz.style.display = 'none'
    getText()
},2000)
})
///// Our Amazing Work      ////////
const amazWorkUl = document.querySelector('.amazUl')
const amazImg = document.getElementsByClassName('amaz-contentImg')
amazWorkUl.addEventListener('click', function (elem){
    for(let i = 0; i < amazImg.length ; i++){
        if (i < amazWorkUl.children.length){
        amazWorkUl.children[i].classList.remove('amazLiActive')
        }
        amazImg[i].style.display = ''
        if (elem.target.getAttribute('name') === 'amazAll'){
            amazImg[i].style.display = ''
        } else if (elem.target.getAttribute('name') !== amazImg[i].getAttribute('name')){
            amazImg[i].style.display = 'none'
        }
    }
    elem.target.classList.add('amazLiActive')
})
////////////////////
function getText (){
    let hoverCategory = document.getElementsByClassName('hover_category')
    for( let i = 0; i < amazImg.length; i++){
        if (hoverCategory[i].parentNode.parentNode.getAttribute('name') === 'gd'){
            hoverCategory[i].textContent = 'Graphic Design'
        } if (hoverCategory[i].parentNode.parentNode.getAttribute('name') === 'wd'){
            hoverCategory[i].textContent = 'Web Design'
        }if (hoverCategory[i].parentNode.parentNode.getAttribute('name') === 'lp'){
            hoverCategory[i].textContent = 'Landing Pages'
        }if (hoverCategory[i].parentNode.parentNode.getAttribute('name') === 'wp'){
            hoverCategory[i].textContent = 'Wordpress'
        }
    }
}
getText()
///////////////////// Карусель :)
//                     Часть1
const ObjCandidates = {

    amy:{
        skills: `he initially was discovered by Raj and Howard as a possible match for Sheldon through an online dating service. By Sheldon's own admission, she is most like him by any standard. Like him, she has previously avoided relationships (whether romantic or otherwise is entirely unclear).`,
        name: 'Amy Farrah Fowler',
        occupation: `neuroscientist`,
        src: `./img/people/foto/emy.jpg`,
    },

    sheldon:{
        skills: `Sheldon makes a speech to a class of bored and disinterested grad students, reminding them that they will never be as smart as he is. Teaching is something else that Sheldon fails at having no social skills.`,
        name: `Dr. Sheldon Lee Cooper`,
        occupation: `theoretical physicist`,
        src: `./img/people/foto/Sheldon_Cooper.jpg`,
    },

    howard:{
        skills: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.`,
        name: `HOWARD WOLOWITZ`,
        occupation: `UX Designer`,
        src: `./img/people/foto/Howard_Wolowitz.jpg`
    },

    penny:{
        skills:`An aspiring, but mostly unsuccessful actress, she was born on a farm near Omaha, Nebraska, and worked at The Cheesecake Factory as a waitress until she quit in "The Occupation Recalibration" (S7E13) to take up acting full-time`,
        name: `Penny Hofstadter`,
        occupation:`Actress (Formerly)`,
        src:`./img/people/foto/Penny(1).jpg`
    },
}
function removePeopleActive(){
    for (let i=0;i<allCandidates.children.length; i++){
        allCandidates.children[i].classList.remove('people-active')
    }
}
const photoFrame = document.querySelector('.photoFrame')
function addClass(){
    skills.classList.add('people-rgba')
    occupation.classList.add('people-rgba')
    peopleName.classList.add(`people-name-rgba`)
    photoFrame.style.animationName = 'picture-size'
}
function delAnimation () {
    setTimeout(function (){
    photoFrame.style.animationName = ''
},500) }
function removeClass (){
    skills.classList.remove('people-rgba')
    occupation.classList.remove('people-rgba')
    peopleName.classList.remove('people-name-rgba')
}
function amy (elem){
    addClass()
    elem.parentNode.classList.add('people-active')
    function emy () {
        skills.textContent = ObjCandidates.amy.skills
        peopleName.textContent = ObjCandidates.amy.name
        occupation.textContent = ObjCandidates.amy.occupation
        mainFoto.src = ObjCandidates.amy.src
        removeClass()
    }
    setTimeout(emy,250)
    delAnimation()
}
function sheldon(elem){
    addClass()
    elem.parentNode.classList.add('people-active')
    function sheldon(){
    skills.textContent = ObjCandidates.sheldon.skills
    peopleName.textContent = ObjCandidates.sheldon.name
    occupation.textContent = ObjCandidates.sheldon.occupation
    mainFoto.src = ObjCandidates.sheldon.src
    removeClass()
    }
    setTimeout(sheldon,250)
    delAnimation()
}

function howard (elem){
    addClass()
    elem.parentNode.classList.add('people-active')
    function howard() {
        skills.textContent = ObjCandidates.howard.skills
        peopleName.textContent = ObjCandidates.howard.name
        occupation.textContent = ObjCandidates.howard.occupation
        mainFoto.src = ObjCandidates.howard.src
        removeClass()
    }
    setTimeout(howard,250)
    delAnimation()
}
function penny (elem){
    addClass()
    elem.parentNode.classList.add('people-active')
    function penny() {
        skills.textContent = ObjCandidates.penny.skills
        peopleName.textContent = ObjCandidates.penny.name
        occupation.textContent = ObjCandidates.penny.occupation
        mainFoto.src = ObjCandidates.penny.src
        removeClass()
    }
    setTimeout(penny,250)
    delAnimation()
}

const allCandidates = document.querySelector('.allCandidates')
const skills = document.querySelector('.skills')
const peopleName = document.querySelector('.name')
const occupation = document.querySelector('.occupation')
const mainFoto = document.querySelector('.mainFoto')
allCandidates.addEventListener('click', function (elem){
    let elemName = elem.target.getAttribute('name')
    // console.log(even.target.tagName)
    if (elem.target.tagName === 'UL' || elem.target.tagName === 'LI'){
    return
    }
    removePeopleActive()
    elem = elem.target
    if (elemName === 'amy'){
        amy (elem)
    }
    if (elemName === 'sheldon'){
        sheldon(elem)
    }
    if (elemName === 'howard'){
        howard (elem)
    }
    if (elemName === 'penny'){
        penny(elem)
    }
})
const btnLeft = document.getElementsByClassName('switch')[0]
const btnRight = document.getElementsByClassName('switch')[1]
btnLeft.addEventListener('click', function (elem){
    let iStart = 0
    for (let i=0;i<allCandidates.children.length;i++){
        if (allCandidates.children[i].classList.contains('people-active')){
            iStart = i
        }
    }
    removePeopleActive()
    iStart -= 1
    if (iStart< 0){
        iStart = 3
    }
    let elemName = (allCandidates.children[iStart].getAttribute('name'))
    let elemImg = allCandidates.children[iStart].children[0]
    console.log(allCandidates.children[iStart].children[0])
    console.log(elemName)
    if (elemName === 'amy'){
        amy (elemImg)
    }
    if (elemName === 'sheldon'){
        sheldon(elemImg)
    }
    if (elemName === 'howard'){
        howard (elemImg)
    }
    if (elemName === 'penny'){
        penny(elemImg)
    }
})

btnRight.addEventListener('click', function (){
    let iStart = 0
    for (let i=0;i<allCandidates.children.length;i++){
        if (allCandidates.children[i].classList.contains('people-active')){
            iStart = i
        }
    }
    removePeopleActive()
    iStart += 1
    if (iStart> 3){
        iStart = 0
    }
    let elemName = (allCandidates.children[iStart].getAttribute('name'))
    let elemImg = allCandidates.children[iStart].children[0]
    console.log(allCandidates.children[iStart].children[0])
    console.log(elemName)
    if (elemName === 'amy'){
        amy (elemImg)
    }
    if (elemName === 'sheldon'){
        sheldon(elemImg)
    }
    if (elemName === 'howard'){
        howard (elemImg)
    }
    if (elemName === 'penny'){
        penny(elemImg)
    }
})































// elem.target.tagName === 'UL'
// if (elem.target.tagName==='svg'){
//     elem = elem.target.parentNode
// }else if (elem.target.tagName==='path'){
//     elem = elem.target.parentNode.parentNode
// } else {
//     elem = elem.target
// }










