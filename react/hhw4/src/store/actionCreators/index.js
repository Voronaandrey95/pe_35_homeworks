import { INIT_CARD, INIT_FAVOURITE, PUSH_BODY_MODAL, CONTROL_MODAL, INIT_CART,MINUS_COUNTER,DELETE_COUNTER} from "../actions";

/////////////////////      cardReduser

export const initCards=()=> async (dispatch)=>{
    // const localItem =  localStorage.getItem('items')&& localStorage.getItem('items').length>0? JSON.parse(localStorage.getItem('items')) : false; 
    let localItem = localStorage.getItem('items');
    if (localItem!==null){ 
      localItem = JSON.parse(localItem)  
    //   setItems(localItem)
    dispatch({type: INIT_CARD, payload: localItem})
    }else{
        
    (async()=>{
      const {data}= await fetch ('items.json')
        .then(response=>response.json())
        dispatch({type: INIT_CARD, payload: data})
        localStorage.setItem('items', JSON.stringify(data))
  }
    )()}
}

export const initFavourite = (id)=> (dispatch)=>{
    dispatch({type: INIT_FAVOURITE, payload: id})
}

export const initCart = (id)=> (dispatch)=>{
  dispatch({type: INIT_CART, payload: id})
}

export const minusCounte = (id)=>(dispatch)=>{
  dispatch({type: MINUS_COUNTER, payload: id})
}
export const deleteCounter = (id)=>(dispatch)=>{
  dispatch({type: DELETE_COUNTER, payload: id})
}

/////////////////////     modalReduser
export const controlModal = (item)=> (dispatch)=>{
  dispatch({type:CONTROL_MODAL})
}
export const pushBodyModal = (item)=>(dispatch)=>{
  dispatch({type:PUSH_BODY_MODAL, payload: item })
}
/////////////////////      cartReduser

export const pushCart =(item)=>(dispatch)=>{
}

