/////////////////////      cardReduser

export const INIT_CARD = "INIT_CARD"
export const INIT_FAVOURITE = "INIT_FAVOURITE"
export const INIT_CART = "INIT_CART"
export const MINUS_COUNTER = "MINUS_COUNTER"
export const DELETE_COUNTER = "DELETE_COUNTER"

/////////////////////      modalReduser
export const CONTROL_MODAL ="CONTROL_MODAL"
export const PUSH_BODY_MODAL = "PUSH_BODY_MODAL"

/////////////////////      cartReduser
export const PUSH_CART = "PUSH_CART"