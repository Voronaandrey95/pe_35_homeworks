import { CONTROL_MODAL, PUSH_BODY_MODAL} from "../actions";

const initialState={
    isOpen:false,
    body:[]
}
    
export const modalReduser = (state = initialState, {type, payload})=>{
    switch (type) {

        case CONTROL_MODAL:{
            const bullet = !state.isOpen
            return {...state, isOpen: bullet}
        }
        case PUSH_BODY_MODAL:{
            let newBody= state.body
            newBody = [payload]
            return({...state, body: newBody})
        }
        
        default:
            {return state}
    }
} 