import { PUSH_CART} from "../actions";

const initialState={
    data: []
}
    
export const cartReduser = (state = initialState, {type, payload})=>{
    switch (type) {
        
        case PUSH_CART: {
            const newData = [...state.data]
            const index = (payload) =  newData.findIndex(({id : arrId}) =>{
              return payload === arrId
            })
            //   const newItems = [...items];
            newData[index].isFavourite = !newData[index].isFavourite ;   
            localStorage.setItem('items', JSON.stringify(newData))
            console.log(newData[index].isFavourite);
            return {...state, data: newData}
        }
        default:
            {return state}
    }
} 