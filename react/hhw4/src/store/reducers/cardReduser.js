import { INIT_CARD, INIT_FAVOURITE, INIT_CART, MINUS_COUNTER, DELETE_COUNTER } from "../actions";

const initialState={
    data: []
}
    
export const cardReduser = (state = initialState, {type, payload})=>{
    switch (type) {
         case INIT_CARD:{
            return {...state, data: payload}
        }
        case INIT_FAVOURITE: {
            const newData = [...state.data]
            const index = (payload) =  newData.findIndex(({id : arrId}) =>{
              return payload === arrId
            })
            //   const newItems = [...items];
            newData[index].isFavourite = !newData[index].isFavourite ;   
            localStorage.setItem('items', JSON.stringify(newData))
            console.log(newData[index].isFavourite);
            return {...state, data: newData}
        }
        
        case INIT_CART: {
            const newData = [...state.data]
            const index = (payload) =  newData.findIndex(({id : arrId}) =>{
              return payload === arrId
            })
            if(!newData[index].counterCart || newData[index].counterCart === 0){
            newData[index].isCard = true;
            newData[index].counterCart = 1
            } else{
            newData[index].counterCart =newData[index].counterCart + 1
            }
            console.log(newData[index]);
            localStorage.setItem('items', JSON.stringify(newData))
            console.log(newData[index].isCard);
            return {...state, data: newData}
        }
        case MINUS_COUNTER: {
            const newData = [...state.data]
            const index = (payload) =  newData.findIndex(({id : arrId}) =>{
              return payload === arrId
            })
            if(newData[index].counterCart > 0){
            newData[index].counterCart = newData[index].counterCart -1
            } else {
            newData[index].isCard = false ;
            newData[index].counterCart = 0
            }
            console.log(newData[index]);
            localStorage.setItem('items', JSON.stringify(newData))
            console.log(newData[index].isCard);
            return {...state, data: newData}
        }
        case DELETE_COUNTER: {
            const newData = [...state.data]
            const index = (payload) =  newData.findIndex(({id : arrId}) =>{
              return payload === arrId
            })
            newData[index].isCard = false ;
            newData[index].counterCart = 0
            console.log(newData[index]);
            localStorage.setItem('items', JSON.stringify(newData))
            console.log(newData[index].isCard);
            return {...state, data: newData}
        }
        

        default:
            {return state}
    }
} 