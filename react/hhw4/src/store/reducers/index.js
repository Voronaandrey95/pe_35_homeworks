import { combineReducers } from "redux";
import { cardReduser } from "./cardReduser";
import { cartReduser } from "./cartReduser";
import { modalReduser } from "./modalReduser";

const reducer = combineReducers({
    card: cardReduser,
    cart: cartReduser,
    modal: modalReduser
    
})

export default reducer
