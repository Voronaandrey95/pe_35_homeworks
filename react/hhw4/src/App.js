import React ,{useEffect, useState} from 'react';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import Routes from "./Routes";
import Header from './components/Header/Header';
import { Provider } from 'react-redux';
import store from './store';

function App() {
  // const [items, setItems]= useState([])
  // const [modal, setModal] = useState({})

  //   const toggleFav = (id) => {
  //      const index = (id) =  items.findIndex(({id : arrId}) =>{
  //       return id === arrId
  //     })
  //       const newItems = [...items];
  //       newItems[index].isFavourite = !newItems[index].isFavourite ;   
  //       setItems(newItems)
  //       localStorage.setItem('items', JSON.stringify(newItems))
  //       console.log(items[index].isFavourite);
  // }


  return (
    <Provider store={store}>
    <BrowserRouter>
      <Header/>
        <Routes />
    </BrowserRouter>
    </Provider>
  );
}

export default App;
