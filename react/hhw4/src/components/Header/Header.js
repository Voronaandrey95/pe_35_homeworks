// import React from 'react';
import styles from './Header.module.scss';
import { Link } from "react-router-dom";

const Header= () => {
        return (
              <header className={styles.root}>
                  <nav>
                      <ul>
                          <li>
                              <Link exact activeClassName={styles.active} to="/">Home</Link>
                          </li>
                          <li>
                              <Link activeClassName={styles.active} to="/cart">Cart</Link>
                          </li>
                          <li>
                              <Link activeClassName={styles.active} to="/favourite">Favourites</Link>
                          </li>
                      </ul>
                  </nav>
              </header>
        );
};

export default Header;
