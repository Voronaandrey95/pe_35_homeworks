import { display } from '@mui/system'
import React from 'react'
import style from "./Modal.module.scss"
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { controlModal, initCart, pushBodyModal  } from '../../store/actionCreators'



const Modal = () =>{

    const {body} = useSelector((store)=> store.modal, shallowEqual)
    console.log(body);
    const dispatch = useDispatch()
    const initModal = ()=>{
        dispatch(controlModal())
    }
    const cart = (id)=>{ 
        dispatch(initCart(id))
    }
    const info=body[0]
    console.log(info);
    // console.log(body);
   
        
    
    return (
        <div onClick={initModal} className={`${style.modal}  ${style.modal_on}`}>
            <div className={style.modal_board}>
                <div className={style.modal_content}>
                    <p>Добаить товар: "{info.name}"?</p>
                    <p>Стоимость: {info.price}</p>
                    <div className={style.modal_buttons}>
                        <button onClick={()=>cart(info.id)} className={`${style.modal_btn_ok} ${style.modal_btn}`}>OK</button>
                        {/* <button className={`${style.modal_btn_no} ${style.modal_btn}`} onClick={initModal}>Cancel</button>  Почему-то срабатывает два раза, открывает и закрывает модальное окно...   */}  
                    </div>
                    {/* <span onClick={closeModal} className={style.modal_closed}></span> */}
                </div>
            </div>
        </div>
    )
}
export default Modal



// const closeModal=()=>{
//     document.body.style.overflow = '';
//  setModal(()=>{
//      return {isOpen: false}
//  })
//  }
//  const setCard = ()=>{
//      const index = (modal.id) =  items.findIndex(({id : arrId}) =>{
//          return modal.id === arrId
//        })
//      const newItems = [...items];
//      if (!newItems[index].isCard){
//      console.log(newItems[index].isCard, index)
//      newItems[index].isCard = true ;
//          if(newItems[index].counterCart === undefined){
//          newItems[index].counterCart = 1
//          }
//      setItems(newItems)
//      localStorage.setItem('items', JSON.stringify(newItems))
//      closeModal()
//      console.log(newItems[index].counterCart)
//      }else{
//          newItems[index].counterCart = newItems[index].counterCart +1;
//          setItems(newItems);
//          localStorage.setItem('items', JSON.stringify(newItems))
//          closeModal()
//      }