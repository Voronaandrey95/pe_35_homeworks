import React from "react"
import style from "./Card.module.scss"
import { StarSvgSelector } from "./StarSvgSelector.tsx";
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { initFavourite, controlModal, pushBodyModal } from "../../store/actionCreators";



const Card = ({item}) =>{

    // const {isOpen} = useSelector((store)=> store.modal, shallowEqual)

    const dispatch = useDispatch()


    const initModal = ()=>{
        dispatch(controlModal())
        dispatch(pushBodyModal(item))
    }
    const favourite = (id)=>{ 
        dispatch(initFavourite(id))
    }



    return(
        <div className={style.cards}>
            <div>
                <img  src={(item.url)} alt={item.id}></img>  
                <div onClick={()=>favourite(item.id)} className={style.svg}>
                <StarSvgSelector   isFavorit={item.isFavourite} id={"STAR"}/>
                </div>
                <p className={style.card_name} >{item.name}</p>
                <p>Price: {item.price}$</p>
                <button  onClick={initModal} className={style.button_card}>Add card</button>
            </div>
        </div>
    )
    }
export default Card



////////////
// const openModal = ()=>{
//     setModal(()=>{
//         const newModal = {...item, isOpen: true}
//         console.log(newModal);
//         return newModal
//     })
//     document.body.style.overflow = 'hidden';
// }
////////////