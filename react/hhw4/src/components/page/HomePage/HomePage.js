import React, { Component, useEffect } from 'react'
import Card from '../../Card/Card'
import style from "./HomePage.module.scss"
import Modal from "../../Modal/Modal"
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { initCards } from '../../../store/actionCreators'



const HomePage =()=>{

    const {data: items} = useSelector((store)=> store.card, shallowEqual)
    const {isOpen} = useSelector((store)=> store.modal, shallowEqual)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(initCards())
    }, [])

    return(
        <section className={style.main_board}>
            {items && items.map(item => <Card item={item} key={item.id} />)} 
            {isOpen &&  <Modal/> }    
        </section>
    )
}
export default HomePage