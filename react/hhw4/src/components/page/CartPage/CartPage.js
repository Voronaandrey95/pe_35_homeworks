import React, { Component, useEffect } from 'react'
import Cart from "../../Cart/Cart"
import style from "./CartPage.module.scss"
import Modal from "../../Modal/Modal"
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { initCards } from '../../../store/actionCreators'



const CartPage =()=>{
    const itemCart = []
    const {data: items} = useSelector((store)=> store.card, shallowEqual)
    console.log(items);
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(initCards())
    }, [])
    items.map(item=>{
        item.isCard && item.counterCart && item.counterCart >0 && itemCart.push(item);
    })
    console.log(itemCart);
    return(
            <section className={style.maim_cart}>
                {itemCart && itemCart.map(item => <Cart item={item} key={item.id}/>)}
            </section>
    )
}
export default CartPage

