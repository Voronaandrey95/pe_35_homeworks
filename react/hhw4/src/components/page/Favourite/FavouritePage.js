import React, { Component, useEffect } from 'react'
import Card from '../../Card/Card'
import style from "./FavouritePage.module.scss"
// import Modal from "../../Modal/Modal"
import { shallowEqual, useSelector, useDispatch } from 'react-redux'
import { initCards } from '../../../store/actionCreators'
import Modal from '../../Modal/Modal'


const FavouritePage =()=>{
    const {data: items} = useSelector((store)=> store.card, shallowEqual)
    const dispatch = useDispatch()
    const {isOpen} = useSelector((store)=> store.modal, shallowEqual)

    useEffect(() => {
        dispatch(initCards())
    }, [])
    const favoriteArr = []
    function favorite(items){
        items.map((item)=>{(item.isFavourite)&&favoriteArr.push(item) })
    }
    favorite(items)
    return(
        <section className={style.main_board}>
            {favoriteArr && favoriteArr.map(item => <Card item={item} key={item.id} />)}  
            {isOpen && <Modal/>}        
        </section>
    )
}
export default FavouritePage