import React ,{useEffect, useState} from 'react';
import { deleteCounter, initCart, minusCounte } from '../../store/actionCreators';
import style from './Cart.module.scss';
import { useDispatch, shallowEqual, useSelector } from 'react-redux';

const Cart = ({item})=>{

    const dispatch = useDispatch()
    const minusCount = (id)=>{ 
        dispatch(minusCounte(id))
    }
    const plusCount = (id)=>{
        dispatch(initCart(id))
    }
    const deleteCount=(id)=>{
        dispatch(deleteCounter(id))
    }
    const buyNow =()=>{
        alert('Give me your money ')
    }
    return(
      
        <div className={style.cart_item}>
            <div className={style.cart_left}>
                <img src={item.url} className={style.cart_img}/>
                <div className={style.carts_description}>
                    <p className={style.cart_name}>{item.name}</p>
                    <p className={style.cart_description}>{item.description}</p>
                </div>
            </div>
            <div className={style.cart_counter}>
                <button onClick={()=>plusCount(item.id)} className={`${style.cart_btn} ${style.cart_btn_plus}`}>+</button>
                <p>{item.counterCart}</p>
                <button onClick={()=>minusCount(item.id)} className={`${style.cart_btn}`}>-</button>
                <div className={style.btn_buyDelete}>
                    <button  onClick={buyNow} className={style.btn_buy}>Buy now</button>
                    <button onClick={()=>deleteCount(item.id)} className={`${style.btn_buy} ${style.btn_delete}` }>Delete now</button>
                </div>

            </div>
            
        </div>
    )
}

export default Cart