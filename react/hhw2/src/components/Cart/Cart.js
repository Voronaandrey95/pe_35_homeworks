import React ,{useEffect, useState} from 'react';
import style from './Cart.module.scss';
const Cart = ({item, setItems, items})=>{

   const[counter, setCounter] = useState(item.counterCart)
    
    const plusCounter = ()=>{
        const newCounter = counter +1
        setCounter(newCounter)
        const newItems = [...items]
        newItems[item.id-1].counterCart = newItems[item.id-1].counterCart +1
        setItems(newItems)
        localStorage.setItem('items', JSON.stringify(newItems))
    }
    const minusCounter = ()=>{
        const newCounter = counter -1
        setCounter(newCounter)
        const newItems = [...items]
        if(newItems[item.id-1].counterCart !== 1){
        newItems[item.id-1].counterCart = newItems[item.id-1].counterCart -1
        setItems(newItems)
        localStorage.setItem('items', JSON.stringify(newItems))
        }else {
            newItems[item.id-1].isCard = false ;
            newItems[item.id-1].counterCart= 1
            setItems(newItems)
        }
    }
    const deleteCounter =()=>{
        setCounter(0)
        const newItems = [...items]
        newItems[item.id-1].counterCart = 1
        newItems[item.id-1].isCard = false ;
        setItems(newItems)
        localStorage.setItem('items', JSON.stringify(newItems))
    }
    const buyNow =()=>{
        alert('Give me your money ')
    }
    return(
      
        <div className={style.cart_item}>
            <div className={style.cart_left}>
                <img src={item.url} className={style.cart_img}/>
                <div className={style.carts_description}>
                    <p className={style.cart_name}>{item.name}</p>
                    <p className={style.cart_description}>{item.description}</p>
                </div>
            </div>
            <div className={style.cart_counter}>
                <button onClick={plusCounter}  className={`${style.cart_btn} ${style.cart_btn_plus}`}>+</button>
                <p>{counter}</p>
                <button onClick={minusCounter} className={`${style.cart_btn}`}>-</button>
                <div className={style.btn_buyDelete}>
                    <button onClick={buyNow} className={style.btn_buy}>Buy now</button>
                    <button onClick={deleteCounter} className={`${style.btn_buy} ${style.btn_delete}` }>Delete now</button>
                </div>

            </div>
            
        </div>
    )
}

export default Cart