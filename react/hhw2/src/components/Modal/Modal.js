import { display } from '@mui/system'
import React from 'react'
import style from "./Modal.module.scss"


const Modal = ({modal,setModal,items, setItems}) =>{

   const closeModal=()=>{
       document.body.style.overflow = '';
    setModal(()=>{
        return {isOpen: false}
    })
    }
    const setCard = ()=>{
        const index = (modal.id) =  items.findIndex(({id : arrId}) =>{
            return modal.id === arrId
          })
        const newItems = [...items];
        if (!newItems[index].isCard){
        console.log(newItems[index].isCard, index)
        newItems[index].isCard = true ;
            if(newItems[index].counterCart === undefined){
            newItems[index].counterCart = 1
            }
        setItems(newItems)
        localStorage.setItem('items', JSON.stringify(newItems))
        closeModal()
        console.log(newItems[index].counterCart)
        }else{
            newItems[index].counterCart = newItems[index].counterCart +1;
            setItems(newItems);
            localStorage.setItem('items', JSON.stringify(newItems))
            closeModal()
        }
        
    }
    return (
        <div className={`${style.modal}  ${modal.isOpen && style.modal_on}`}>
            <div className={style.modal_board}>
                <div className={style.modal_content}>
                    <p>Добаить товар: "{modal.name}"?</p>
                    <p>Стоимость: {modal.price}</p>
                    <div className={style.modal_buttons}>
                        <button onClick={setCard} className={`${style.modal_btn_ok} ${style.modal_btn}`}>OK</button><button className={`${style.modal_btn_no} ${style.modal_btn}`} onClick={closeModal}>Cancel</button>
                    </div>
                    <span onClick={closeModal} className={style.modal_closed}></span>
                </div>
            </div>
        </div>
    )
}
export default Modal