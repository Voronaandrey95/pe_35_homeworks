import React, { Component } from 'react'
import Card from '../../Card/Card'
import style from "./HomePage.module.scss"
import Modal from "../../Modal/Modal"


const HomePage =({items, toggleFav, setModal})=>{
    return(
        <section className={style.main_board}>
            {items && items.map(item => <Card toggleFav={toggleFav}  setModal={setModal} item={item} key={item.id} />)}      
        </section>
    )
}
export default HomePage