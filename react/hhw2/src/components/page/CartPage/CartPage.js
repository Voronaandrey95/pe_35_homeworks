import React, { Component } from 'react'
import Cart from "../../Cart/Cart"
import style from "./CartPage.module.scss"
import Modal from "../../Modal/Modal"



const CartPage =({items, setItems})=>{
    const itemCart = []
    items.map(item => item.isCard&& itemCart.push(item) )
    console.log(itemCart);

    return(
            <section className={style.maim_cart}>
                
                {itemCart && itemCart.map(item => <Cart items={items} setItems={setItems} item={item} key={item.id} itemCart={itemCart}/>)}
            </section>
    )
}
export default CartPage

