import React, { Component } from 'react'
import Card from '../../Card/Card'
import style from "./FavouritePage.module.scss"
import Modal from "../../Modal/Modal"


const FavouritePage =({items, toggleFav, setModal})=>{
    const isFavourite =  []
    items.map((item)=>{
        if(item.isFavourite){
            isFavourite.push(item)
        }
    })  
    return(
        <section className={style.main_board}>
                {items && isFavourite.map((item)=><Card toggleFav={toggleFav} setModal={setModal} item={item} key={item.id} />)}        
        </section>
    )
}
export default FavouritePage