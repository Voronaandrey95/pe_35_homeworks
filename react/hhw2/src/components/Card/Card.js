import React from "react"
import style from "./Card.module.scss"
import { StarSvgSelector } from "./StarSvgSelector.tsx";



const Card = ({item, toggleFav, setModal}) =>{


const openModal = ()=>{
    setModal(()=>{
        const newModal = {...item, isOpen: true}
        console.log(newModal);
        return newModal
    })
    document.body.style.overflow = 'hidden';
}


return(
    <div className={style.cards}>
        <div>
             <img  src={(item.url)} alt={item.id}></img>  
            <div onClick={()=>toggleFav(item.id)} className={style.svg}>
            <StarSvgSelector   isFavorit={item.isFavourite} id={"STAR"}/>
            </div>
            <p className={style.card_name} >{item.name}</p>
            <p>Price: {item.price}$</p>
            <button onClick={openModal} className={style.button_card}>Add card</button>
         </div>
    </div>
)
}

export default Card
