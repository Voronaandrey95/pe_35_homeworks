import React ,{useEffect, useState} from 'react';
import './App.css';
import {BrowserRouter} from "react-router-dom";
import Routes from "./Routes";
import Header from './components/Header/Header';
import Modal from "./components/Modal/Modal"


function App() {
  const [items, setItems]= useState([])
  const [modal, setModal] = useState({})



   useEffect(()=>{
    const localItem =  localStorage.getItem('items')&& localStorage.getItem('items').length>0? JSON.parse(localStorage.getItem('items')) : false;  
    if (localItem){ 
      setItems(localItem)
    }else{
    (async()=>{
      const {data}= await fetch ('./items.json')
        .then(response=>response.json())
        setItems(data)
        localStorage.setItem('items', JSON.stringify(data))
  }
    )()}},[])


    const toggleFav = (id) => {
       const index = (id) =  items.findIndex(({id : arrId}) =>{
        return id === arrId
      })
        const newItems = [...items];
        newItems[index].isFavourite = !newItems[index].isFavourite ;   
        setItems(newItems)
        localStorage.setItem('items', JSON.stringify(newItems))
        console.log(items[index].isFavourite);
  }


  return (
    <BrowserRouter>
      <Header/>
        <Routes toggleFav={toggleFav} modal={modal} setItems={setItems}  setModal={setModal} items={items}/>
        <Modal modal={modal} setItems={setItems} items={items} setModal={setModal}/>
    </BrowserRouter>
  );
}

export default App;
