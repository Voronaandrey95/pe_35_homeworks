import React, {useEffect} from "react";
import {Switch, Route, Redirect, useLocation} from 'react-router-dom'
import HomePage from './components/page/HomePage/HomePage';
import CartPage from './components/page/CartPage/CartPage'
import FavouritePage from './components/page/Favourite/FavouritePage'

import Modal from "./components/Modal/Modal"

const Routes = ({items, toggleFav, setModal, modal, setItems}) => {
    const pathname = useLocation()
    useEffect(() => {
        ;
    }, [pathname]);


   return  (
    <Switch> 
        <Route  path='/favourite'>
            <FavouritePage toggleFav={toggleFav} setModal={setModal} items={items} />
        </Route>

         <Route exact path='/cart'>
                <CartPage  setItems={setItems} items={items}/>
        </Route>

        <Route  path='/'  >
            <HomePage toggleFav={toggleFav} setModal={setModal} items={items} />
        </Route>
        

    </Switch>
)}

export default Routes;