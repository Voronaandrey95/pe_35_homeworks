import logo from './logo.svg';
import './App.scss';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import React, { useState } from "react";

const App =() => {
  const [modalActive, setModalActive]= useState(false)
  const [infoModal,setInfoModal] = useState('sss')
  const info = {
    modal1: {
      text : 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptas libero impedit maxime obcaecati corrupti placeat eveniet atque debitis, consequatur nostrum beatae in nam magnam deserunt expedita veniam recurunt expedita veniam recusandae! Voluptas maxime explicabo veniam quos iure, unde quidem ipsam nisi repellat omnis atque aspernatur consequuntur eum illo vero minima inventore obcaecati odit',
      header : 'Delete component'
    },
    modal2: {
      text : 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius modi enim voluptate consequuntur, earum, voluptas at ab recusandae libero, ex blanditiis. Facere aliquam debitis et nemo vel voluptate vero veritatis corporis aspernatur. Ex aspernatur, soluta voluptatem quasi unde exercitationem consequatur, reiciendis temporibus fuga iste sit debitis sapiente repellendus',
      header : 'Save Component'
    }
  }
    function openModal1() {
      setModalActive(true);
      setInfoModal(info.modal1)
    }
    function openModal2() {
      setModalActive(true);
      setInfoModal(info.modal2)
    }
  
  return (
    <div className="App">
      <div className='buttons'>
        <Button text='Open First Modal' openModal={openModal1}></Button>
        <Button text='Open Second Modal' openModal={openModal2}></Button>
        {/* <Modal></Modal> */}
      </div>
      <Modal active={modalActive} setActive={setModalActive} infoModal={infoModal}/>
    </div>
  );
}
export default App;
