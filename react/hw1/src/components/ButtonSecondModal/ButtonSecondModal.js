import React, { Component } from 'react'
import style from './ButtonSecondModal.module.scss'
export class ButtonSecondModal extends Component {
    render() {
        return (
            <div>
                <button className={style.secondBtn}type='button'>ButtonSecondModal</button>
            </div>
        )
    }
}

export default ButtonSecondModal

