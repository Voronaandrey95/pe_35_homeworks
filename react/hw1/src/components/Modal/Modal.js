import style from './Modal.module.scss'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ButtonModal from './ButtonModal/ButtonModal'

function Modal({active,setActive,infoModal}) {
        
        return (
            <div className={ active ? 'modal active' : 'modal'} onClick={()=>setActive(false)}>
                <div className='modal_content' onClick={e=> e.stopPropagation()}>
                    <div className={style.modal_header}>
                        <p>{infoModal.header}</p>
                        <span className={style.modal_closed} onClick={()=>setActive(false)}>x</span>
                    </div>
                    <div className={style.modal_text} >
                        <p>{infoModal.text}</p> 
                    </div>
                    <div className={style.modal_footer}  >
                        <ButtonModal setActive={setActive} content="Ok"/>
                        <ButtonModal setActive={setActive} content="Cancel"/>
                    </div>

                </div>
            </div>
        )
    
}

export default Modal
