import React, { Component } from 'react'

function  ButtonModal({setActive, content}){

    return(
        <input type="button" onClick={()=>setActive(false)} value={content}></input>
    )

}
export default ButtonModal
