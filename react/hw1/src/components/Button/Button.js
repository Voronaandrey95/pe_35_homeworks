import style from './Button.module.scss'
import React, { Component } from 'react'
import Modal from '../Modal/Modal';
import ReactDOM from 'react-dom';

const Button =({text,openModal})=>{

        return (
            <div>
                <button className= {style[text.replace(/\s/g, '')]} type='button'   onClick={openModal} >
                {text} 
                </button>
            </div>
        )
    
}
export default Button
