const {watch, parallel} = require("gulp");
const {bs} = require("./serv.js");
const {styles} = require("./style.js");
const {scripts} = require("./scripts.js");


const watcher = () => {
    watch("./index.html").on("change", bs.reload)
    watch("./src/js/*.js").on("change", parallel(scripts))
    watch("./src/scss/**/*.scss").on("change", parallel(styles))

};


exports.watcher = watcher