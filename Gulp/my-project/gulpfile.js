// const gulp = require('gulp');

const {serv} = require("./gulp-tasks/serv.js")
const {scripts} = require("./gulp-tasks/scripts.js")
const { watcher } = require("./gulp-tasks/watch.js");
// const {scripts} = require("./gulp-tasks/scripts.js")
const {styles} = require("./gulp-tasks/style.js")
const  {parallel, series} = require('gulp');


exports.default = parallel(serv, watcher, series(styles, scripts))





// gulp.task("test", function (cb){
//     console.log("Task test")
//     cb()
// })

// exports.default =function (cb){
//     console.log("==>> Default task")
//     cb()
// }

// const defaultTask = (cb)=> {
//     serv()
//     cb()
// }
// exports.default = defaultTask